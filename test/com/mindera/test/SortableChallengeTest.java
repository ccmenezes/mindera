package com.mindera.test;

import org.testng.annotations.Test;

import com.mindera.SortableChallenge;

import org.testng.annotations.BeforeTest;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;

public class SortableChallengeTest {
	
	WebDriver driver;
	SortableChallenge sortableChallenge;
	
	@Test
	public void testDrogAndDropCrescent() throws IOException, InterruptedException {
		sortableChallenge.organizeItems();
		sortableChallenge.takeScreenshot();
	}
	
	@BeforeTest
	public void beforeTest() {
		System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");
		driver = new FirefoxDriver();
		sortableChallenge = new SortableChallenge(driver);
		sortableChallenge.goTo();
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
