##QA Sortable Challenge

###Pre-Requisites:

Eclipse IDE.  
JDK 1.8.  
TestNG installed.  
Maven plugin.  
Selenium WebDriver.  
Java.  
Operation System Linux.  

###Run the application

To run the application in the folder where the source code is, use the command line and type 'npm start'.  

###Run the test
Open the Eclipe Ide.  

At the test class SortableChalleng cliks on Run to execute the test case.  