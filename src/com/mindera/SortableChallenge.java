package com.mindera;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SortableChallenge {
	
	private final WebDriver driver;
	private final WebDriverWait wait;
	private List<WebElement> items ;
			
	public void goTo() {
		driver.get("http://localhost:3000/");
	}
	
	public SortableChallenge(final WebDriver driver) {
		this.driver = driver;
	    PageFactory.initElements(driver, this);
	    this.wait = new WebDriverWait(driver, 30);
	}
	
	public void organizeItems() {
		Actions action = new Actions(driver);
		
		items = driver.findElements(By.xpath("//*[@id=\'app\']/ul/li"));
		
		for (int i = 0; i < this.items.size(); i++) {
			for (int j = 1; j < (this.items.size() - i); j++) {
				if (getLineValue(this.items.get(j - 1).getText()) > getLineValue(this.items.get(j).getText())) {
					action.dragAndDrop(this.items.get(j - 1), this.items.get(j));
					action.build().perform();
				}
			}
		}
	}
	
	private int getLineValue(String itemText) {
		return Integer.parseInt(itemText.replaceFirst("Item ", ""));
	}
	
	public List<WebElement> getItems() {
		return items;
	}
	
	public void takeScreenshot() throws IOException, InterruptedException {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("testEvidence/qa-sortable-challenge_test-evidence.png"));
		
		Thread.sleep(5000);
	}
}